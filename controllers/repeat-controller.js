app.controller("repeatController", function ($scope) {
    //the $scope is the model. it acts as the glue between the view (HTML) and the controller.
    //Anything specified on the model is accessible in the view.
    //Anything NOT specified on the model is NOT accessible in the view.

    $scope.name = "John Doe"; // variable name is available in the view.
    var name = "Jane Doe"; // NOT available in the view.

    // declaring an array called directives, which is an array of objects.
    // objects are key, value pairs written in  { }.
    // eg - { name: 'John Doe' } is an object with key = name and value = 'John Doe'
    // objects can have multiple key value pairs. 
    $scope.directives = [{
        name: 'ng-app',
        link: 'https://docs.angularjs.org/api/ng/directive/ngApp'
    }, {
        name: 'ng-controller',
        link: 'https://docs.angularjs.org/api/ng/directive/ngController'
    }, {
        name: 'ng-model',
        link: 'https://docs.angularjs.org/api/ng/directive/ngModel'
    }, {
        name: 'ng-click',
        link: 'https://docs.angularjs.org/api/ng/directive/ngClick'
    }, {
        name: 'ng-repeat',
        link: 'https://docs.angularjs.org/api/ng/directive/ngRepeat'
    }, {
        name: 'ng-if',
        link: 'https://docs.angularjs.org/api/ng/directive/ngIf'
    }, {
        name: 'ng-show',
        link: 'https://docs.angularjs.org/api/ng/directive/ngShow'
    }, {
        name: 'ng-class',
        link: 'https://docs.angularjs.org/api/ng/directive/ngClass'
    }, {
        name: 'ng-src',
        link: 'https://docs.angularjs.org/api/ng/directive/ngSrc'
    }, {
        name: 'ng-change',
        link: 'https://docs.angularjs.org/api/ng/directive/ngChange'
    }]
})